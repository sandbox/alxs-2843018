<?php

/**
 * @file
 * Callbacks for viewing entities.
 */

/**
 * Entity view callback.
 */
function audio_manager_view($entity, $view_mode='full') {

  drupal_set_title(entity_label('audio_manager', $entity));
  $entity_type = $entity->entityType();
  $entity_id = entity_id($entity_type, $entity);
  $entity->content = array();
  $entity->title = filter_xss($entity->title);
  drupal_add_js(libraries_get_path('soundmanager2') . '/soundmanager2.js');
  //
  // Build the fields content
  //
  field_attach_prepare_view($entity_type, array($entity_id => $entity), $view_mode);
  entity_prepare_view($entity_type, array($entity_id => $entity));
  $entity->content += field_attach_view($entity_type, $entity, $view_mode);
  $entity->content += array(
    '#theme'     => 'audio_manager__item',
    '#element'   => $entity,
    '#view_mode' => $view_mode,
    '#language'  => LANGUAGE_NONE,
  );
  drupal_add_css(drupal_get_path('module', 'audio_manager') . '/audio-manager.css', array(
    'type' => 'file',
    'preprocess' => FALSE,
  ));

  return $entity->content;

}
