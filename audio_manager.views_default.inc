<?php

/**
 * @file
 * Views exported from Drupal with "Export" button.
 */

/**
 * Implements hook_views_default_views().
 */
function audio_manager_views_default_views() {
  $items = new view();
  $items->name = 'audio_manager_list';
  $items->description = '';
  $items->tag = 'default';
  $items->base_table = 'audio_manager';
  $items->human_name = 'Audio Files';
  $items->core = 7;
  $items->api_version = '3.0';
  $items->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $items->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Audio Files';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'view audio manager content';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '<h3>Upload</h3>';
  $handler->display->display_options['header']['area']['content'] .= '<a class="button success" href="' . url("audio-manager/add") . '">Add Audio File</a>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  /* Field: Typical Entity 6: Custom entity v6 ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'audio_manager';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  /* Field: Typical Entity 6: Custom entity v6 title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'audio_manager';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'audio-manager/[id]';
  /* Field: Typical Entity 6: Custom entity v6 description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'audio_manager';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Edit';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Edit';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'audio-manager/[id]/edit';

  /* Display: Page */
  $handler = $items->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = TRUE;
  $handler->display->display_options['path'] = 'audio-manager';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Audio Manager';
  $handler->display->display_options['menu']['weight'] = '0';

  $views['audio_manager_list'] = $items;

  return $views;
}

